package au.com.lib


import jenkins.model.*
import hudson.model.*
import com.cloudbees.hudson.plugins.folder.*
import jenkins.branch.*
import org.jenkinsci.plugins.workflow.job.*
import org.jenkinsci.plugins.workflow.multibranch.*


def deleteOldBuilds(item, Integer numberOfBuildsToKeep, Integer numberOfSuccessfulBuildsKept) {
    def count = 1

    println('Iterating old builds')

    def buildList = []


    for (build in item.getBuilds()) {
      println('PROCESSING ..... ' + build)

        if(count >= numberOfBuildsToKeep) {
            if(item.getBuildStatusIconClassName() == 'icon-blue' && numberOfSuccessfulBuildsKept == 0) {
                println('Not deleting build' + build)
            } else {
                
                    
          
                   // println('Deleting -------------- ' + build)
                    buildList.add(build)
                 // build.delete()

            
            }
        } else if(item.getBuildStatusIconClassName() == 'icon-blue') {
            numberOfSuccessfulBuildsKept++
        }
        count = count +1; 

    }
    
        for (build in buildList) {
                                println('Deleting -------------- ' + build)

            build.delete()

        }
    
    println ('done')
    //println('Prior Build Count: (' + count + ')')
    //println ''
}



def cleaupOldBuilds(item, Integer numberOfBuildsToKeep, Integer numberOfSuccessfulBuildsKept) {
    
    if (numberOfBuildsToKeep <=5 || numberOfSuccessfulBuildsKept <1 ){
        println('Number of builds to keep cant be <5  and number of successfull build to be maintained cant be < 1')
    }
    else{
        if(item instanceof Project) {
            println('Processing Project: (' + item.getName() + ')')
                                            println('Processing Project: ljsdsldjsdljsdljsd')

            deleteOldBuilds(item, numberOfBuildsToKeep, numberOfSuccessfulBuildsKept)
        } else if(item instanceof Folder) {
            println 'wwewewewdssffff'
            println('Processing Folder: (' + item.getName() + ')')
            for (subItem in item.items) {
                                println('Processing Project: FFFFffff')

                cleaupOldBuilds(subItem, numberOfBuildsToKeep, numberOfSuccessfulBuildsKept)
            }
        } else if(item instanceof WorkflowMultiBranchProject) {
            println('Processing Multi-Branch-Project: (' + item.getName() + ')')
                            println('Processing Project: wwwwwwwwwwwwwwwww')

            for (subItem in item.items) {
                println('Processing Project: wwwwwwwwwwwwwwwww')

                cleaupOldBuilds(subItem, numberOfBuildsToKeep, numberOfSuccessfulBuildsKept)
            }
        }  else if(item instanceof WorkflowJob) {
                println('Processing Project: wvsfsfsfwyiwhw')

            println('Processing Multi-Branch-Job: (' + item.getName() + ')')
            deleteOldBuilds(item, numberOfBuildsToKeep, numberOfSuccessfulBuildsKept)
                            println('Processing Project: DONEwvsfsfsfwyiwhw ')

        } else if(item instanceof OrganizationFolder) {
            println('Processing Org-Folder: (' + item.getName() + ')')
            for (subItem in item.items) {
                println('Processing Project: wwwwwwwwwwwwwwwww')
                println('Processing Project: wwwwwwwwwwwwwwwww')

                cleaupOldBuilds(subItem, numberOfBuildsToKeep, numberOfSuccessfulBuildsKept)
            }
        } else {
            println('Unknown item found to process: (' + item.getName() + ')')
            println('Class: (' + item.getClass() + ')')
            println('Inspect: (' + item.inspect() + ')')
        }
    }
}


return this


